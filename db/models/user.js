var config = require('./../index');
var Sequelize = require('sequelize');
const lockModel = require('./lock');
//console.log(config.connection);
const User = config.connection.define('user', {
    // id: Sequelize.INTEGER,
    name: {
        type: Sequelize.STRING,
        field: 'name'
    },
    username: {
        type: Sequelize.STRING(60),
        field: 'username'
    },
    password: {
        type: Sequelize.STRING,
        field: 'password'
    },
      dob: {
        type: Sequelize.STRING(30),
        field: 'dob'
    }
});
 // User.hasMany(lockModel,{sourceKey:'user_id'});

User.sync({ force: false })
	.then(function(err) {
		console.log('It worked!');
	}, function (err) {
		console.log('An error occurred while creating the table:', err);
});



module.exports = User;