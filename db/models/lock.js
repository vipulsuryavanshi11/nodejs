var config = require('./../index');
var Sequelize = require('sequelize');
const userModel = require('./user');
//console.log(config.connection);
const Lock = config.connection.define('lock', {
    name: {
        type: Sequelize.STRING(50),
        field: 'name'
    },
    macId: {
        type: Sequelize.STRING,
        field: 'macId'
    },
    user: { 
        type: Sequelize.INTEGER,
        field: 'user'
    }
});
// Lock.belongsTo(userModel,{targetKey : 'id'});

Lock.sync({ force: false })
	.then(function(err) {
		console.log('It worked!');
	}, function (err) {
		console.log('An error occurred while creating the table:', err);
});

module.exports = Lock;