const express = require('express');
const jwt = require('jsonwebtoken');
var env = require('./../../config/env');
var User = require('./../../db/models/user');
const bcrypt = require('bcrypt');
const router = express.Router();

//User Registration
router.post('/add', function(req, res){
    req.assert('username', 'Username is required').notEmpty();
    req.assert('password', 'Password is required').notEmpty();

    var errors = req.validationErrors();

    if(!errors){
        var user_details = {
            username:req.body.username,
            //password:cryptr.encrypt(req.body.password)
        }
        User.findOne({where:{username:req.body.username}}).then(user=>{
             if(!user){
                bcrypt.hash(req.body.password, 10, function(err, hash) {
                    if(hash){
                        user_details.password = hash;
                        User.create(user_details).then( function(user){
                            if(user){
                                 res.json({status: true, data:user, message: 'User Created Sucessfully'});
                            }else{
                               res.json({status: false, message: 'Server Error'});
                           }
                        });
                    }else{
                        res.json({status:false,message:'server Error'});
                    }
                
                });
                    
            }else{
                res.json({status: false, message: 'User Already Exist'});
            }

        });

        }else {
        var error_msg = '';
        errors.forEach(function(error) {
            error_msg += error.msg + ',';
        })
        res.json({ status: false, message: error_msg });
    }
});

router.post('/login', function(req, res) {
    req.assert('username', 'username is required').notEmpty();
    req.assert('password', 'password is required').notEmpty();

    var errors = req.validationErrors();
    if (!errors) {
        var password = req.body.password;
        var username = req.body.username;  

        User.findOne({where:{username:username}}).then(user=>{
            if(user){
                bcrypt.compare(password, user.password, function(err, result) {
                    if(result){
                        var token = jwt.sign ({ id: user.id }, env.secretKey.secret, { expiresIn: 86400 });
                        res.json({status: true, token: token, message: 'sucessfully signIn'});
                    }else{
                        res.json({status: false, message: 'signIn Failed Please Provide Correct Cridentuals'});
                    }    
               
                });
               
            }else{
                res.json({status: false, message: 'signIn Failed Please Provide Correct Cridentuals'});
            }
        });
    }
    else {
        var error_msg = '';
        errors.forEach(function(error) {
            error_msg += error.msg + ',';
        })
        res.json({ status: false, message: error_msg });
    }
});

module.exports = router;