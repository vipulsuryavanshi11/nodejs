const express = require('express');
var env = require('./../../config/env');
var User = require('./../../db/models/user');
const router = express.Router();

const VerifyToken = require('./../middlewares/middlewares');


//user Update
router.put('/update', VerifyToken, function(req, res) {

    req.assert('name', 'name is required').notEmpty();
    req.assert('dob', 'dob is required').notEmpty();

    var errors = req.validationErrors();

    if (!errors) {
        
        var user = { 'name': req.body.name, 'dob': req.body.dob }

        User.update(user,{where:{id:req.user_id}}).then( function(user){
            if(user){
                 res.json({status: true, message: 'User Updated Sucessfully'});
            }else{
               res.json({status: false, message: 'User Not Found'});
            }
        });
    }else {
        var error_msg = '';
        errors.forEach(function(error) {
            error_msg += error.msg + ',';
        })
        res.json({ status: false, message: error_msg });
    }
});
    

router.patch('/patch', VerifyToken, function(req, res) {
 
    var user = req.body;

    User.update(user,{where:{id:req.user_id}}).then( function(user){
        if(user){
             res.json({status: true, message: 'User Updated Sucessfully'});
        }else{
           res.json({status: false, message: 'User Not Found'});
        }
    });
});


router.delete('/delete', VerifyToken, function(req, res) {
    User.destroy({where:{id:req.user_id}}).then( function(user){
        if(user){
             res.json({status: true, data:user, message: 'User Deleted Sucessfully'});
        }else{
           res.json({status: false, message: 'User Not Found'});
        }
    });
});


router.get('/me', VerifyToken, function(req, res) {
    User.findAll({where:{id:req.user_id}}).then( function(user){
        if(user){
             res.json({status: true, data:user, message: 'User Details'});
        }else{
           res.json({status: false, message: 'User Not Found'});
        }
    });
});

router.get('/users', VerifyToken, function(req, res) {
    User.findAll().then( function(user){
        if(user){
             res.json({status: true, data:user, message: 'All Users Details'});
        }else{
           res.json({status: false, message: 'server error'});
        }
    });
});


module.exports = router;

