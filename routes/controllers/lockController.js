const express = require('express');
var env = require('./../../config/env');
var User = require('./../../db/models/lock');
const router = express.Router();
const uuid = require('uuid/v1');
const VerifyToken = require('./../middlewares/middlewares');

router.post('/add', VerifyToken, function(req, res) {
    req.assert('name', 'Lock Name is required').notEmpty();

    var errors = req.validationErrors();

    if (!errors) {
        var lock_details = { 'macId': uuid(), 'name': req.body.name, 'user': req.user_id }
        Lock.findOne({where:{name:req.body.name}}).then(lock=>{
            if(lock){
                 res.json({ status: false, data: lock, message: 'Lock name already exists' });
            }else{
                Lock.create(lock_details).then( function(lock){
                    if(lock){
                         res.json({status: false, data:lock, message: 'lock Created Sucessfully'});
                    }else{
                       res.json({status: false, message: 'Server Error'});
                    }
                });
            }
        });
        
    }else {
        var error_msg = '';
        errors.forEach(function(error) {
             error_msg += error.msg + ',';
        })
        res.json({ status: false, message: error_msg });
    }
});

router.put('/update', VerifyToken, function(req, res) {
    req.assert('name', 'Lock Name is required').notEmpty();


    var lock = { 'name': req.body.name }
    var errors = req.validationErrors();

    if (!errors) {
        var lock_details = {'name': req.body.name};

        Lock.update(lock_details,{where:{id:req.body.lock_id}}).then( function(lock){
                if(lock){
                     res.json({status: true, message: 'Lock Updated Sucessfully'});
                }else{
                   res.json({status: false, message: 'Lock Not Found'});
                }
            });
        }
        else {
            var error_msg = '';
            errors.forEach(function(error) {
                error_msg += ',';
            })
            res.json({ status: false, message: error_msg });
        }
});

router.delete('/delete', VerifyToken, function(req, res) {
    Lock.destroy({where:{id:req.body.lock_id}}).then( function(lock){
        if(lock){
             res.json({status: true, message: 'Lock Deleted Sucessfully'});
        }else{
           res.json({status: false, message: 'Lock Not Found'});
        }
    });
});

router.get('/get', VerifyToken, function(req, res) {
    Lock.findAll({where:{user:req.user_id}}).then( function(lock){
        if(lock){
             res.json({status: true, data:lock, message: 'User locks'});
        }else{
           res.json({status: false, message: 'Lock Not Found'});
        }
    });
});
module.exports = router;
